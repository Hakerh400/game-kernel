'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const O = require('../omikron');

const cwd = __dirname;
const abbrevsFile = path.join(cwd, 'abbrevs.txt');

const arr2obj = arr => {
  const obj = O.obj();
  
  for(const [key, val] of arr)
    obj[key] = val;
  
  return obj;
};

const abbrevs = arr2obj(O.sanl(O.rfs(abbrevsFile, 1)).
  map(a => a.split(' ').reverse()));

module.exports = abbrevs;