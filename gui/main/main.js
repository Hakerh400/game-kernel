'use strict';

const fs = require('fs');
const path = require('path');
const assert = require('../assert');
const cp = require('child_process');
const O = require('../omikron');
const abbrevs = require('./abbrevs');

const {min, max} = Math;

const cwd = __dirname;
const projDir = path.join(cwd, '../..');
const exeFile = path.join(projDir, 'main');
const thDir = path.join(projDir, 'theory');
const cssFile = path.join(cwd, 'style.css');

const tabSize = 2;
const tab = ' '.repeat(tabSize);
const thExt = 'liza';

const parens = '()[]{}'.split('');

const main = () => {
  addStyle(cssFile);

  const html = O.doc.documentElement;
  const mainDiv = O.ceDiv(O.body, 'main');
  
  const openFileDialog = O.ceDiv(mainDiv, 'open-file-dialog');
  const filePthInput = O.ceInput(openFileDialog, 'text', 'file-pth');
  const filesDiv = O.ceDiv(openFileDialog, 'files');
  
  const editor = O.ceDiv(mainDiv, 'editor');
  const lineNumsDiv = O.ceDiv(editor, 'line-nums');
  const codeWrap = O.ceDiv(editor, 'code-wrap');
  const infoViewWrap = O.ceDiv(editor, 'info-view-wrap');
  const infoView = O.ceDiv(infoViewWrap, 'info-view');
  const code = O.ceDiv(codeWrap, 'code-div');
  const ta = O.ce(codeWrap, 'textarea');
  ta.classList.add('code-ta');
  
  let curFile = '';
  
  const getFilePth = () => {
    const pth = filePthInput.value.split(/[\/\\]/);
    const name = pth.pop();
    const dir = path.join(thDir, pth.join('/'));
    
    return [dir, name];
  };
  
  const setFiles = files => {
    filesDiv.innerText = files.join('\n');
  };
  
  const calcFiles = () => {
    const [dir, name] = getFilePth();
    
    if(!fs.existsSync(dir)) return;
    if(!fs.statSync(dir).isDirectory()) return;
    
    const files = fs.readdirSync(dir).map(name => {
      if(name.endsWith(`.${thExt}`))
        return name.slice(0, -(thExt.length + 1));
      
      if(name.includes('.')) return null;
      
      return `${name}/`;
    }).filter(m => {
      if(m === null) return 0;
      return m.includes(name);
    }).sort((m1, m2) => {
      return m2.endsWith('/') - m1.endsWith('/') ||
        (m1 < m2 ? -1 : m1 > m2);
    });
    
    return files;
  };
  
  const updateFiles = () => {
    const files = calcFiles() ?? [];
    setFiles(files);
  };
  
  const openFile = () => {
    const [dir, name] = getFilePth();
    const file = path.join(dir, `${name}.${thExt}`);
    
    if(!fs.existsSync(dir))
      fs.mkdirSync(dir, {recursive: true});
    
    if(!fs.existsSync(file))
      O.wfs(file, '');
    
    curFile = file;
    const str = O.rfs(file, 1);
    
    showEditor(str);
  };
  
  const saveFile = () => {
    const str = ta.value.trimRight();
    O.wfs(curFile, str);
  };
  
  const onPthKeyDown = evt => {
    const {code} = evt;
    const flags = O.evtFlags(evt);
    
    if(flags === 0){
      if(code === 'Enter'){
        O.pd(evt);
        openFile();
        return;
      }
    }
  };

  const onBlur = () => {
    saveFile();
  };

  O.ael(ta, 'blur', onBlur);
  
  O.ael(filePthInput, 'input', updateFiles);
  O.ael(filePthInput, 'keydown', onPthKeyDown);
  
  const getPartsRaw = () => {
    const str = ta.value;
    const selStart = ta.selectionStart;
    const selEnd = ta.selectionEnd;
    
    return [
      str.slice(0, selStart),
      str.slice(selStart, selEnd),
      str.slice(selEnd),
    ];
  };
  
  const setPartsRaw = (s1, s2, s3) => {
    const selStart = s1.length;
    const selEnd = selStart + s2.length;

    ta.value = s1 + s2 + s3;
    ta.setSelectionRange(selStart, selEnd);
  };
  
  const getParts = () => {
    const [s1, s2, s3] = getPartsRaw();
    if(s2 !== '') return null;
    return [s1, s3];
  };
  
  const setParts = (s1, s2) => {
    setPartsRaw(s1, '', s2);
  };
  
  let selStartPrev = 0;
  let selEndPrev = 0;
  let codeChanged = 1;
  
  const updateEditor = () => {
    html.scrollTop = 0;
    html.scrollLeft = 0;
    
    const selStart = ta.selectionStart;
    const selEnd = ta.selectionEnd;
    
    const changed = codeChanged || selStart !== selStartPrev || selEnd !== selEndPrev;
    if(!changed) return;
    
    const parts = getParts();
    
    if(parts !== null){
      const [s1, s2] = parts;
      setParts(s1, s2.trimRight() + '\n'.repeat(100));
    }
    
    const str = ta.value;
    const lines = O.sanl(str);
    
    selStartPrev = selStart;
    selEndPrev = selEnd;
    codeChanged = 0;
    
    // ta.style.height = `max(100%, 16px*${lines.length})`;
    code.innerText = '';
    
    const selLen = selEnd - selStart;
    const hasSel = selLen !== 0;

    let pos = 0;

    for(const line of lines){
      const lineLen = line.length;
      const posNext = pos + lineLen + 1;
      
      const lineDiv = O.ceDiv(code, 'line');

      const txt = (str, sel) => {
        const div = O.ceDiv(lineDiv, 'txt');
        if(sel) div.classList.add('selected');
        div.innerText = str;
        return div;
      };

      const checkSel = sel => {
        return sel >= pos && sel < posNext;
      };

      if(checkSel(selStart) || checkSel(selEnd)){
        if(hasSel){
          if(selStart <= pos){
            const offset = selEnd - pos;

            txt(line.slice(0, offset), 1);
            txt(line.slice(offset));
          }else{
            const offset1 = max(selStart - pos, 0);
            const offset2 = max(selEnd - pos, 0);
            
            txt(line.slice(0, offset1));
            txt(line.slice(offset1, offset2), 1);
            txt(line.slice(offset2));
          }
        }else{
          const offset = max(selStart - pos, 0);

          txt(line.slice(0, offset))
          
          const cur = O.ceDiv(lineDiv, 'cursor');
          cur.innerText = ' ';
          
          txt(line.slice(offset));
        }
      }else{
        txt(line || ' ', selStart <= pos && selEnd >= posNext);
      }

      pos = posNext;
    }
    
    showLinesNum: {
      const [s1, s2, s3] = getPartsRaw();
      const linesNum = lines.length - O.sanl(s3).length + O.sanl(s3.trimRight()).length;
      
      lineNumsDiv.innerText = O.ca(linesNum, i => i + 1).join('\n') + '\n'.repeat(100);
    }
    
    let scrollTop = ta.scrollTop;
    scrollTop = scrollTop - scrollTop % 19;
    
    code.scrollTo(0, scrollTop);
    lineNumsDiv.scrollTo(0, scrollTop);
    
    // const cur = O.ce(code, 'div');
    // cur.classList.add('cursor');
    // cur.innerText = ' '
  };
  
  const updateEditorWrap = () => {
    updateEditor();
    O.raf(updateEditorWrap);
  };
  
  O.raf(updateEditorWrap);
  
  O.ael(ta, 'input', evt => {
    codeChanged = 1;
  });
  
  const areParenChars = (c1, c2) => {
    const index = parens.indexOf(c1);
    
    if(index === -1 || index % 2 !== 0) return 0;
    if(c2 !== parens[index + 1]) return 0;
    
    return 1;
  };
  
  const onKeyDown = evt => {
    const {code, key} = evt;
    const flags = O.evtFlags(evt);

    if(flags === 4){
      if(code === 'KeyW'){
        saveFile();
        showOpenFileDialog();
        return 0;
      }
      
      if(code === 'Enter'){
        saveFile();
        
        console.clear();
        
        const res = cp.spawnSync(exeFile, [filePthInput.value]);
        const out = O.sanll('\n' + O.lf(res.stdout.toString()));
        const err = O.sanl(res.stderr.toString()).
          filter(a => !a.startsWith('---> ')).join('\n');
        
        infoView.innerText = err ||
          (out.length === 1 ? 'ok' : out.slice(2).join('\n\n'));
        
        return 0;
      }
      
      if(code === 'KeyZ'){
        codeChanged = 1;
        return;
      }
    }
    
    const parts = getParts();
    if(parts === null) return;
    
    const [s1, s2] = parts;
    const sr = O.rev(s1);
    const line1Rev = sr.match(/^.*/)[0];
    const line1 = O.rev(line1Rev);
    const line2 = s2.match(/^.*/)[0];
    const ind = line1.match(/^(?: {2})*(?:\- )?/)[0].length;
    const c1 = O.last(s1);
    const c2 = s2 !== '' ? s2[0] : null;
    
    if(flags === 0 || flags === 2){
      checkParens: {
        const index = parens.indexOf(key);
        
        if(index === -1) break checkParens;
        
        if(index % 2 === 0){
          if(/^\S/.test(s2) && parens.indexOf(s2[0]) == -1)
            return [s1 + key, s2];
          
          return [s1 + key, parens[index + 1] + s2];
        }
        
        if(!s2.startsWith(key)) break checkParens;
        
        return [s1 + key, s2.slice(1)];
      }
      
      if(code === 'Backspace'){
        if(areParenChars(c1, c2))
          return [s1.slice(0, -1), s2.slice(1)];
        
        if(ind !== 0 && ind === line1.length)
          return [s1.slice(0, -2), s2];
      }
      
      if(code === 'Enter'){
        if(areParenChars(c1, c2))
          return [`${s1}\n${indent(ind + tabSize)}`, `\n${indent(ind)}${s2}`];
        
        return [`${s1}\n${indent(ind)}`, s2];
      }
      
      const tryToSubstAbbrev = s => {
        const match = line1Rev.match(/^([^\\]+)\\/);
        if(match === null) return null;
        
        const str = O.rev(match[1]);
        if(!O.has(abbrevs, str)) return null;
        
        return [s1.slice(0, -(str.length + 1)) + abbrevs[str] + s, s2];
      };
      
      if(code === 'Tab'){
        const res = tryToSubstAbbrev('');
        if(res !== null) return res;
        
        return [s1 + tab, s2];
      }
      
      if(code === 'Space'){
        const res = tryToSubstAbbrev(' ');
        if(res !== null) return res;
      }
      
      if(isDigit(key)){
        if(O.last(s1) === '\\'){
          setParts(s1 + key, s2);
          
          return onKeyDown({...evt,
            code: 'Tab',
            key: 'Tab',
            ctrlKey: 0,
            shiftKey: 0,
            altKey: 0,
          });
        }
      }
      
      // if(code === 'PageUp' || code === 'PageDown')
      //   return 0;
      
      return;
    }
    
    // ctrl
    if(flags === 4){
      if(code === 'ArrowUp'){
        ta.scrollTop -= 19;
        return 0;
      }
      
      if(code === 'ArrowDown'){
        ta.scrollTop += 19;
        return 0;
      }

      if(code === 'KeyX'){
        const n1 = line1.length;
        const n2 = line2.length;
        let s1New = n1 !== 0 ? s1.slice(0, -n1) : s1;
        
        setParts(s1New, s2.slice(n2 + 1));

        return 0;
      }
    }
    
    // ctrl + shift
    if(flags === 6){
      if(code === 'KeyD')
        return [`${s1}${line2}\n${line1}`, s2];
      
      return;
    }
  };
  
  O.ael(ta, 'keydown', evt => {
    codeChanged = 1;
    
    const res = onKeyDown(evt) ?? null;
    
    if(res !== null){
      O.pd(evt);
      if(res !== 0) setParts(...res);
    }
  });
  
  O.ael(ta, 'scroll', evt => {
    codeChanged = 1;
  });
  
  const showOpenFileDialog = () => {
    O.hide(editor);
    O.show(openFileDialog);
    
    filePthInput.focus();
    
    updateFiles();
  };
  
  const showEditor = str => {
    O.hide(openFileDialog);
    O.show(editor);
    
    ta.value = str;
    ta.setSelectionRange(0, 0);
    ta.focus();
    infoView.innerText = '';
    
    codeChanged = 1;
  };
  
  if(0){
    filePthInput.value = 'other/ap/defs';
    openFile();
  }else{
    showOpenFileDialog();
  }
};

const addStyle = cssFile => {
  const style = O.ce(O.head, 'style');
  style.innerHTML = O.rfs(cssFile, 1);
};

const indent = len => {
  return ' '.repeat(len);
};

const isDigit = c => c >= '0' && c <= '9';

const isSubDigit = c => c >= '₀' && c <= '₉';

main();