import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except

import Util
import Bit
import Grid
import Data
import Game

main :: IO ()
main = do
  run init_state

show_tile :: Tile -> String
show_tile d = if _tile_player d
  then "#" else "."

show_grid :: Grid Tile -> String
show_grid grid = unlines' # map concat # _grid_tiles #
  grid_map grid show_tile

show_board :: Board -> String
show_board b = show_grid # _board_grid b

show_state :: S -> String
show_state s = show_board # _s_board s

run :: S -> IO ()
run s = do
  putStrLn # show_state s
  putStrLn ""
  inp <- getLine
  putStrLn ""
  let b = _s_board s
  let grid = _board_grid b
  let dir = (read inp :: N)
  if not # dir >= 0 && dir <= 3 then run s else do
    let Just (d, x, y) = grid_find grid # \d x y -> _tile_player d
    let (d', x', y') = grid_nav grid x y dir
    case d' of
      Nothing -> run s
      Just d' -> do
        grid <- pure # grid_set grid x y # d {_tile_player = False}
        grid <- pure # grid_set grid x' y' # d' {_tile_player = True}
        s <- pure # s {_s_board = b {_board_grid = grid}}
        run s