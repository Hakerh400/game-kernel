module Grid where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except

import Util
import Bit

data Grid a = Grid
  { _grid_w :: N
  , _grid_h :: N
  , _grid_tiles :: [[a]]
  } deriving (Eq, Ord, Read, Show)

grid_mk :: N -> N -> (N -> N -> a) -> Grid a
grid_mk w h f = Grid
  { _grid_w = w
  , _grid_h = h
  , _grid_tiles = mk_list h # \y -> mk_list w # \x -> f x y
  }

grid_modify :: Grid a -> ([[a]] -> [[b]]) -> Grid b
grid_modify grid f = grid {_grid_tiles = f # _grid_tiles grid}

grid_map :: Grid a -> (a -> b) -> Grid b
grid_map grid f = grid_modify grid # map # map f

grid_has :: Grid a -> N -> N -> Prop
grid_has grid x y = x >= 0 && x < _grid_w grid &&
  y >= 0 && y < _grid_h grid

grid_get :: Grid a -> N -> N -> Maybe a
grid_get grid x y = do
  row <- list_get' y # _grid_tiles grid
  list_get' x row

grid_set :: Grid a -> N -> N -> a -> Grid a
grid_set grid x y d = if not # grid_has grid x y then grid
  else grid_modify grid # modify_at y # set_at x d

dir_to_dif :: N -> (N, N)
dir_to_dif dir = case dir of
  0 -> (0, -1)
  1 -> (1, 0)
  2 -> (0, 1)
  3 -> (-1, 0)

dir_to_nav :: N -> N -> N -> (N, N)
dir_to_nav x y dir = let
  (dx, dy) = dir_to_dif dir
  in (x + dx, y + dy)

grid_nav :: Grid a -> N -> N -> N -> (Maybe a, N, N)
grid_nav grid x y dir = let
  (x', y') = dir_to_nav x y dir
  in (grid_get grid x' y', x', y')

grid_find_aux' :: (a -> N -> N -> Prop) -> N -> N -> [a] -> Maybe (a, N, N)
grid_find_aux' f x y row = case row of
  [] -> Nothing
  (d:row) -> if f d x y
    then Just (d, x, y)
    else grid_find_aux' f (x + 1) y row

grid_find_aux :: (a -> N -> N -> Prop) -> N -> N -> [[a]] -> Maybe (a, N, N)
grid_find_aux f x y rows = case rows of
  [] -> Nothing
  (row:rows) -> case grid_find_aux' f x y row of
    Nothing -> grid_find_aux f x (y + 1) rows
    res -> res

grid_find :: Grid a -> (a -> N -> N -> Prop) -> Maybe (a, N, N)
grid_find grid f = grid_find_aux f 0 0 # _grid_tiles grid