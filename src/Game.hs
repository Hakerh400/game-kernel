module Game where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except

import Util
import Bit
import Grid
import Data

init_grid :: Grid Tile
init_grid = grid_mk 5 5 # \x y -> Tile
  { _tile_player = x == 0 && y == 0
  }

init_board :: Board
init_board = Board
  { _board_grid = init_grid
  }

init_state :: S
init_state = S
  { _s_turn = 0
  , _s_board = init_board
  }