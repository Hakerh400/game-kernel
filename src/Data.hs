module Data where

import qualified Data.Set as Set
import qualified Data.Map as Map
import Data.Set (Set)
import Data.Map (Map)

import Data.Char
import Data.List
import Data.Maybe
import Data.Either
import Data.Bits
import Data.Word
import Data.Functor.Identity
import Data.Time.Clock
import Control.Applicative
import Control.Monad
import Control.Monad.State
import Control.Monad.Trans.Except

import Util
import Bit
import Grid

data Tile = Tile
  { _tile_player :: Prop
  } deriving (Eq, Ord, Read, Show)

data Board = Board
  { _board_grid :: Grid Tile
  } deriving (Eq, Ord, Read, Show)

data S = S
  { _s_turn :: N
  , _s_board :: Board
  } deriving (Eq, Ord, Read, Show)