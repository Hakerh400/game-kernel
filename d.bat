@echo off
cls

cd src
ghc -O -outputdir "../build" -o "../main.exe" -prof -fprof-auto Main.hs
cd ..